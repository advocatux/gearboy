# Gearboy Color

This is a fork of Ryan Pattison's Gearboy port for Ubuntu Touch which is a fork of Ignacio Sanchez's Gearboy, focusing on Qt/QML and OpenGL ES for mobile phones.

<img src="platforms/ubuntu_touch/gearboy.png" width="64">
<img src="platforms/ubuntu_touch/screenshots/landscape.png" width="256">
<img src="platforms/ubuntu_touch/screenshots/portrait.png" height="256">

An Ubuntu Touch version is available in [OpenStore](https://open-store.com/app/gearboy.bhdouglass)

## Build Instructions

### Ubuntu Touch

- [Install Clickable](http://clickable.bhdouglass.com/en/latest/)
- `clickable -c platforms/ubuntu_touch/clickable.json`
- Clickable will compile the project and install it on your connected device

### Android & Desktop

Build these with Qt Creator and the project files under platforms. These versions exist to test on a variety of hardware and are not (yet) supported.
